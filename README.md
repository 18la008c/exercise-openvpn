# exercise-openvpn

## how to use
1. `docker-compose up -d`
2. When all containers are up, do bellow command at local pc.
    ```
    route add -net <vpnIP@client> netmask <netmask> gw <opvn-serverIP@private>
    ```
    ex.) static
    ```
    route add -net 10.10.0.20 netmask 255.255.255.255 gw 10.9.0.10
    ```
    ex.) PublicKeyInfrastructure
    ```
    route add -net 10.10.0.0 netmask 255.255.255.0 gw 10.9.0.10
    ```