cd /usr/share/easy-rsa
yes | ./easyrsa init-pki
cp vars.example vars
echo -ne "\n" | ./easyrsa build-ca nopass
./easyrsa build-server-full ovpn-server nopass
./easyrsa build-client-full client1 nopass
./easyrsa build-client-full client2 nopass
./easyrsa gen-dh